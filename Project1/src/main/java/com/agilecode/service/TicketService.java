package com.agilecode.service;

import com.agilecode.entity.Ticket;

import java.util.List;

public interface TicketService {
    List<Ticket> listTickets();
}


