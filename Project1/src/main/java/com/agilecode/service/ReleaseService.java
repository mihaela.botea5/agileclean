package com.agilecode.service;

import com.agilecode.entity.Release;

import java.util.List;

public interface ReleaseService {
    List<Release> listReleases();
}


